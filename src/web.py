#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @author Gabriel Lopez
from flask import Flask, request, g, redirect, render_template, jsonify
from flask.ext.babel import Babel
from flask.ext.babel import gettext as _
import sqlite3
import smtplib
from email.mime.text import MIMEText
from wtforms.form import Form
from wtfrecaptcha.fields import RecaptchaField


app = Flask(__name__)
app.config.from_object("config")
#  config babel
babel = Babel(app)


@babel.localeselector
def get_locale():
    return g.lang


class CaptchaForm(Form):
    """Utility class for captcha validation."""
    captcha = RecaptchaField(
        public_key=app.config["RECAPTCHA_PUB_KEY"],
        private_key=app.config["RECAPTCHA_PRIV_KEY"],
        secure=True)


# db utilities
def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def make_dicts(cur, row):
    return dict((cur.description[idx][0], value)
                for idx, value in enumerate(row))


@app.before_request
def before_request():
    lang = request.args.get("lang")
    if not lang:
        lang = request.accept_languages.best_match(
            app.config["LANGUAGES"].keys())
    g.lang = lang
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


# routes!
@app.route("/")
@app.route("/work/<int:work_id>")
def home(work_id=None):
    cur = g.db.execute(
        "SELECT * FROM works where lang=? ORDER BY id DESC", (g.lang,))
    works = [make_dicts(cur, row) for row in cur.fetchall()]

    if work_id is None:
        return render_template("welcome.html", works=works)

    elif works[0]["id"] <= work_id <= works[-1]["id"]:
        for work in works:
            if work["id"] == work_id:
                post = work
                break

    else:
        cur = g.db.execute("SELECT * FROM works WHERE id=? and lang=?",
                           (work_id, g.lang))
        row = cur.fetchone()
        if row is None:
            return render_template("works/not-found.html", works=works)
        post = make_dicts(cur, row)

    return render_template("works/{}.html".format(post["id"]),
                           works=works, post=post)


@app.route("/blog/<int:post_id>")
@app.route("/blog/")
@app.route("/blog")
def blog(post_id=None):
    cur = g.db.execute(
        "SELECT * FROM blog WHERE lang=? ORDER BY id DESC LIMIT 10",
        (g.lang,))
    entries = [make_dicts(cur, row) for row in cur.fetchall()]

    # if no post_id is supplies, show the most recent post
    if post_id is None:
        return redirect("/blog/{}?lang={}"
                        .format(entries[0]["id"], g.lang))

    elif entries[0]["id"] <= post_id <= entries[-1]["id"]:
        for entry in entries:
            if entry["id"] == post_id:
                post = entry
                break

    else:
        cur = g.db.execute("SELECT * FROM blog WHERE id=? AND lang=?",
                           (post_id, g.lang))
        row = cur.fetchone()
        if row is None:
            return render_template("blog/not-found.html", entries=entries)
        post = make_dicts(cur, row)

    return render_template("blog/{}.html".format(post["id"]),
                           entries=entries, post=post)


@app.route("/contact")
def contact():
    return render_template("contact.html",
                           captcha=app.config["RECAPTCHA_PUB_KEY"])


@app.route("/email", methods=["POST"])
def email():
    form = CaptchaForm(
        request.form, captcha={'ip_address': request.remote_addr})

    # is captcha valid?
    if form.validate():
        # send email!
        msg = MIMEText(request.form["message"])
        msg["From"] = request.form["address"]
        msg["To"] = app.config["MAIL_RECV"]
        msg["Subject"] = "Conctact from blog."
        s = smtplib.SMTP('localhost')
        try:
            s.send_message(msg)
            s.quit()
        except:
            return jsonify(success=False)
        return jsonify(success=True)

    else:
        return jsonify(error=_("Invalid captcha"))


@app.route('/<path:path>')
def catch_all(path):
    """Page not found."""
    return render_template("not-found.html")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
