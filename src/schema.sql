-- -*- mode: sql; sql-product: sqlite; -*-
-- _id is the primary key
-- id and lang are the funtional indexes

create table blog (
    _id integer primary key,
    id integer not null,
    lang char(2) not null,
    title varchar(20) not null,
    category varchar(20) not null,
    tstamp integer not null,
    description varchar(500) not null
    );

insert into blog (id, lang, title, category, tstamp, description) values
  (1, 'en', 'What is this site?' , 'info', date(),
    'Who is injaon and What will you find here?'),
  (1, 'es', '¿Qué es este sitio?' , 'info', date(),
    '¿Quién es injaon y qué encontraras aqui?');


create table works (
    _id integer primary key,
    id integer not null,
    lang char(2) not null,
    title varchar(20) not null,
    category varchar(20) not null,
    description varchar(500) not null
    );


insert into works (id, lang, title, category, description) values
  (1, 'en', 'Tutor', 'Education', "Calculus I & II's tutor in the university."),
  (1, 'es', 'Tutor', 'Education', "Tutor de Calculo I & II en la universidad."),
  (2, 'en', 'Web Developer', 'Software',
    'Creation of an inscription site for a national congress'),
  (2, 'es', 'Desarrollo Web', 'Software',
    'Creación de sitio de inscripciones para congreso nacional'),
  (3, 'en', 'JS Developer', 'Software',
  'Contribution to Fidus Writer, an Open Sourece alternative to Google Docs.'),
  (3, 'es','Desarrollador JS', 'Software',
  'Contribución a Fidus Writer, una alternativa de Código Abierto a Google Docs.'),
  (4, 'en', 'This BLOG!', 'Software', 'Creation of this awesome site.'),
  (4, 'es', 'Este BLOG!', 'Software', 'Creación de este fantastico sitio.');
