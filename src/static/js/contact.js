$(document).ready(function () {
    $("#emailForm").on("submit", function(evnt) {
        var data = {};
        data.address = $("#contactEmail").val();
        data.message = $("#contactMessage").val();
        data.recaptcha_response_field = $("#recaptcha_response_field").val();
        data.recaptcha_challenge_field = $("#recaptcha_challenge_field").val();

        if (data.recaptcha_response_field == "")
            $("#captcha-error").removeClass("hidden").html("Empty Captcha");

        else {
            $.post("/email", data, function(data) {
                if (data.error)
                    $("#captcha-error").removeClass("hidden").html(data.error);

                else {
                    // valid captcha!
                    $("#captcha-error").addClass("hidden");
                    if (data.success)
                        alert("Email enviado!");
                    else
                        alert("Email No enviado. Intente mas tarde");
                    $("#emailForm")[0].reset();
                }
                Recaptcha.reload();

            }, "json").fail(function () {
                alert("FAIL: Email No enviado. Intente mas tarde");
                Recaptcha.reload();
            });
        }
        evnt.preventDefault();
    });
});
