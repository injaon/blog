My personal blog using the next technologies:

### Backend

* Python 3.3
* Flask
* SQLite

### Frontend

* Bootstrap 3.0.0
* Jquery 1.10.2
* [malosanu 1.1](https://wrapbootstrap.com/theme/malosanu-personal-blog-portfolio-WB0905P3F)
*(It's not Free Software but it's very cheap)*


# Installation

Fetch the repo

`git clone https://injaon@bitbucket.org/injaon/blog.git`

Create and activate the virtual enviroment

`cd blog`

`virtualenv -p python3 venv`

`source venv/bin/activate`

## Dependencies

`pip install flask flask-babel wtforms-recaptcha gunicorn`

## Run it!

`cd src; gunicorn web:app`
